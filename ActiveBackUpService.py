import os
import win32file
import win32con
import shutil
import errno
import logging
from time import sleep
from datetime import datetime
import sys


# Create dictionary with number as key
ACTIONS = {
    1: "Created",
    2: "Deleted",
    3: "Updated",
    4: "Renamed from something",
    5: "Renamed to something"
}

FILE_LIST_DIRECTORY = 0x0001
dataPathFromCmd = sys.argv[1]
backupPathFromCmd = sys.argv[2]

dataDir = sys.argv[1] if os.path.exists(sys.argv[1]) else r"D:\\testing_area\\staging"
backupDir = sys.argv[2] if os.path.exists(sys.argv[1]) else r"D:\\testing_area\\backup\\"
defaultErrorLogPath = (os.path.join(os.environ['USERPROFILE'], "Desktop"))

defaultErrorLogger = logging.getLogger("defaultLogger")
defaultErrorLogger.setLevel(logging.DEBUG)

# create file log handler and set level to debug
fh = logging.FileHandler(os.path.join(defaultErrorLogPath, "ActiveBackUpServiceError.log"))
fh.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
defaultErrorLogger.addHandler(fh)

# code below is for logging all other errors - to be handler in future.
try:
    if not os.path.exists(dataDir):
        defaultErrorLogger.error("Source Directory does not exist!")
        raise FileNotFoundError(
            errno.ENOENT, os.strerror(errno.ENOENT), dataDir)
    elif not os.path.exists(backupDir):
        defaultErrorLogger.error("Destination Directory does not exist!")
        raise FileNotFoundError(
            errno.ENOENT, os.strerror(errno.ENOENT), backupDir)
except:
    pass
logFormat = "%(asctime)s :%(message)s"
logging.getLogger("root")
logging.basicConfig(filename=os.path.join(backupDir, 'BackupLog_' + (datetime.now().isoformat(timespec='milliseconds'))
                                          .replace(":", "_") + '.log'), level=logging.DEBUG, format=logFormat)
fileWaitTime = 0.1
exception_count = 0
# hDir is called as a handle to monitor the directory changes
# format: win32file.CreateFile(filename (in this case dir name_, desiredAccess, sharedMode, attributes,
#  creationDistribution, flagsAndAttributes, hTemplateFile)
hDir = win32file.CreateFile(
    dataDir,
    FILE_LIST_DIRECTORY,
    win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE | win32con.FILE_SHARE_DELETE,
    None,
    win32con.OPEN_EXISTING,
    win32con.FILE_FLAG_BACKUP_SEMANTICS,
    None
)
logger = logging.getLogger('BackupService')
while 1:
    #
    # ReadDirectoryChangesW takes a previously-created
    # handle to a directory, a buffer size for results,
    # a flag to indicate whether to watch subtrees and
    # a filter of what changes to notify.
    #
    # NB Tim Juchcinski reports that he needed to up
    # the buffer size to be sure of picking up all
    # events when a large number of files were
    # deleted at once.
    #
    results = win32file.ReadDirectoryChangesW(
        hDir,
        1024,
        True,
        win32con.FILE_NOTIFY_CHANGE_FILE_NAME |
        win32con.FILE_NOTIFY_CHANGE_DIR_NAME |
        win32con.FILE_NOTIFY_CHANGE_ATTRIBUTES |
        win32con.FILE_NOTIFY_CHANGE_SIZE |
        win32con.FILE_NOTIFY_CHANGE_LAST_WRITE |
        win32con.FILE_NOTIFY_CHANGE_SECURITY,
        None,
        None

    )
    try:
        for action, file in results:
            full_filename = os.path.join(dataDir, file)
            logger.debug(": %s %s", full_filename, ACTIONS.get(action, "Unknown"))

            if ACTIONS.get(action) is "Renamed from something":
                old_file = file
            elif ACTIONS.get(action) is "Renamed to something":
                new_file = file
                print(old_file, "renamed to", new_file)
                try:
                    os.rename(os.path.join(backupDir, old_file), os.path.join(backupDir, new_file))
                except FileExistsError:
                    pass
            elif ACTIONS.get(action) is "Updated":
                if os.path.isfile(os.path.join(dataDir, file)):
                    shutil.copy(os.path.join(dataDir, file), os.path.join(backupDir, file))
            elif ACTIONS.get(action) is "Deleted":
                if os.path.isfile(os.path.join(backupDir, file)):
                    os.remove(os.path.join(backupDir, file))
                else:
                    shutil.rmtree(os.path.join(backupDir, file))
            else:
                copiedFlag = False
                while copiedFlag is not True:
                    try:
                        if os.path.isfile(os.path.join(dataDir, file)):
                            shutil.copy(os.path.join(dataDir, file), os.path.join(backupDir, file))
                        else:
                            shutil.copytree(os.path.join(dataDir, file), os.path.join(backupDir, file))
                        copiedFlag = True
                        exception_count += 1
                        # ** = exponent
                        if exception_count > 3:
                            fileWaitTime = exception_count ** 1.4 / 10
                    except OSError as exc:
                        if exc.errno == errno.EACCES:
                            sleep(fileWaitTime)
    except OSError as evx:
        pass
