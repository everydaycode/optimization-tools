import filecmp
import logging
import os
import errno
import shutil
from datetime import datetime


class DirectoryFileComparison:
    @staticmethod
    def build_files_set(root_dir):
        files_set = set()
        for (dirPath, dirName, fileNames) in os.walk(root_dir):
            for fileName in fileNames + dirName:
                full_path = os.path.join(dirPath, fileName)
                relativepath = os.path.relpath(full_path, root_dir)
                files_set.add(relativepath)

        return files_set

    def compare_directories(self, dir1, dir2):
        files_set1 = self.build_files_set(dir1)
        files_set2 = self.build_files_set(dir2)
        return files_set1 - files_set2, files_set2 - files_set1

    def are_dir_trees_equal(self, dir1, dir2):
        """
        Compare two directories recursively. Files in each directory are
        assumed to be equal if their names and contents are equal.

        @param dir1: First directory path
        @param dir2: Second directory path

        @return: True if the directory trees are the same and
            there were no errors while accessing the directories or files,
            False otherwise.
       """

        dirs_cmp = filecmp.dircmp(dir1, dir2)

        if len(dirs_cmp.left_only) > 0 or len(dirs_cmp.right_only) > 0 or \
                len(dirs_cmp.funny_files) > 0:
            return False

        (_, mismatch, errors) = filecmp.cmpfiles(
            dir1, dir2, dirs_cmp.common_files, shallow=False)
        if len(mismatch) > 0 or len(errors) > 0:
            return False
        for common_dir in dirs_cmp.common_dirs:
            new_dir1 = os.path.join(dir1, common_dir)
            new_dir2 = os.path.join(dir2, common_dir)
            if not self.are_dir_trees_equal(new_dir1, new_dir2):
                return False
        return True


''' 
    Code snippet by Mateusz Kobos, stackoverflow
    Will be used for final check to see if backup done correctly
'''

# dataPathFromCmd = sys.argv[1]
# backupPathFromCmd = sys.argv[2]
# sys.argv[1] if os.path.exists(sys.argv[1]) else


dataDir = r"\\ENVYPC\Users\Kedar\Music\staging"
# sys.argv[2] if os.path.exists(sys.argv[2]) else
backupDir = r"\\ENVYPC\Users\Kedar\Pictures\backup"
homepath = (os.path.join(os.environ['USERPROFILE'], "Desktop"))


class GenerateLogger:
    @staticmethod
    def generate_default_logger(loggername, level, defaultpath, logfilename):
        try:
            if not os.path.exists(defaultpath):
                raise NotADirectoryError(errno.ENOENT, os.strerror(errno.ENOENT), defaultpath)
        except OSError:
            raise RuntimeError()
        newlogger = logging.getLogger(loggername)
        newlogger.setLevel(level)
        newfilehandler = logging.FileHandler(os.path.join(defaultpath, logfilename))
        newfilehandler.setLevel(level)
        newformatter = logging.Formatter('%(asctime)s - %(message)s')
        newfilehandler.setFormatter(newformatter)
        newlogger.addHandler(newfilehandler)
        return newlogger


try:
    errorlogger = GenerateLogger.generate_default_logger("defaultErrorLogger",
                                                         logging.DEBUG, homepath, "defaultError.log")
    if not os.path.exists(dataDir):
        errorlogger.error("Source Directory does not exist!")
        raise NotADirectoryError(
            errno.ENOENT, os.strerror(errno.ENOENT), dataDir)
    elif not os.path.exists(backupDir):
        errorlogger.error("Destination Directory does not exist!")
        raise NotADirectoryError(
            errno.ENOENT, os.strerror(errno.ENOENT), backupDir)
except OSError:
    pass

filecopylogger = GenerateLogger.generate_default_logger("filecopylogger",
                                                        logging.DEBUG, backupDir, "IncrementalBackup_" +
                                                        (datetime.now().isoformat(timespec='milliseconds'))
                                                        .replace(":", "_") + '.log')


if __name__ == "__main__":
    dirc = DirectoryFileComparison()
    in_dir1, in_dir2 = dirc.compare_directories(dataDir, backupDir)

    print("Files copied from Data:")
    for relative_path in in_dir1:
        print(relative_path)
        if os.path.isfile(os.path.join(dataDir, relative_path)):
            if not os.path.exists(os.path.join(backupDir, relative_path)):
                os.makedirs(os.path.dirname(os.path.join(backupDir, relative_path)), exist_ok=True)
                filecopylogger.info(" File copied: %s", relative_path)
                shutil.copy(os.path.join(dataDir, relative_path), os.path.join(backupDir, relative_path))
            else:
                shutil.copy(os.path.join(dataDir, relative_path), os.path.join(backupDir, relative_path))
        else:
            os.makedirs(os.path.join(backupDir, relative_path), exist_ok=True)

    for relative_path in in_dir2:
        print(relative_path)
